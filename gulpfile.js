// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var inject = require('gulp-inject');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var jscs = require('gulp-jscs');
var gutil = require('gulp-util');

// Check code syntax
gulp.task('review', function() {
  return gulp.src('app/**/*.js')
    .pipe(jscs())
    .pipe(jscs.reporter());
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src('app/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Concatenate & Minify JS
gulp.task('minify-scripts', function() {
    return gulp.src(['./app/core/core.module.js',
                    './app/**/!(*app).module.js',
                    './app/app.module.js',
                    './app/**/*.js', '!app/**/*.test.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify().on('error', gutil.log))
        .pipe(gulp.dest('dist/js'));
});

// Move node_modules
gulp.task('move-node-modules', function() {
    return gulp.src(['./node_modules/angular/angular.js',
                   './node_modules/angular-ui-router/release/angular-ui-router.js',
                   './node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
                   './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
                   './node_modules/firebase/firebase.js',
                   './node_modules/angularfire/dist/angularfire.min.js'])
        .pipe(gulp.dest('dist/vendor/js'))
});

// Move bower_components
gulp.task('move-bower-components', function() {
    gulp.src(['./bower_components/firebase/firebase.js',
              './bower_components/jquery/dist/jquery.min.js',
              './bower_components/paper/dist/paper-core.min.js',
              './bower_components/angular-messages/angular-messages.min.js',
              './bower_components/angularUtils-pagination/dirPagination.js',
              './bower_components/angular-sanitize/angular-sanitize.min.js',
              './bower_components/ng-notify/dist/ng-notify.min.js',
              './bower_components/ng-lodash/build/ng-lodash.min.js',
              './bower_components/ng-csv/build/ng-csv.js'])
      .pipe(gulp.dest('dist/vendor/js'));

    gulp.src('./bower_components/bootstrap/fonts/*.*')
      .pipe(gulp.dest('dist/vendor/fonts'));

    return gulp.src(['./bower_components/bootstrap/dist/css/bootstrap.min.css',
                    './bower_components/bootstrap/dist/css/bootstrap-theme.min.css'])
      .pipe(gulp.dest('dist/vendor/css'));
});

// CSS
gulp.task('minify-styles', function() {
    return gulp.src('./assets/stylesheets/**/*.css')
        .pipe(minifyCSS({comments:true, spare:true}))
        .pipe(gulp.dest('dist/css'));
});

// Fill index
gulp.task('build-index', function() {
  var target = gulp.src('./index.html');
  var sources = gulp.src(['node_modules/angular/angular.js',
                          'node_modules/angular-ui-router/release/angular-ui-router.js',
                          'node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
                          'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
                          'node_modules/firebase/firebase.js',
                          'node_modules/angularfire/dist/angularfire.min.js',
                          'bower_components/firebase/firebase.js',
                          'bower_components/jquery/dist/jquery.min.js',
                          'bower_components/bootstrap/dist/css/bootstrap.min.css',
                          'bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
                          'bower_components/angular-messages/angular-messages.min.js',
                          'bower_components/angularUtils-pagination/dirPagination.js',
                          'bower_components/paper/dist/paper-core.min.js',
                          'bower_components/angular-sanitize/angular-sanitize.min.js',
                          'bower_components/ng-csv/build/ng-csv.js',
                          'bower_components/ng-notify/dist/ng-notify.min.js',
                          'bower_components/ng-notify/src/styles/ng-notify.css',
                          'bower_components/ng-lodash/build/ng-lodash.min.js',
                          './app/app.module.js',
                          './app/core/core.module.js',
                          './app/**/*.module.js',
                          './app/**/*.js',
                          '!./app/**/*.test.js',
                          './assets/**/*.css'], {read: false});

  return target.pipe(inject(sources, {relative: true}))
      .pipe(gulp.dest('./'));

});

// Move Images
gulp.task('move-images', function() {
    return gulp.src('./assets/img/**/*')
        .pipe(gulp.dest('dist/assets/img'));
});

// Move Index
gulp.task('move-index', function() {
    return gulp.src('./index.html')
        .pipe(gulp.dest('dist'));
});

// Move html
gulp.task('move-html', function() {
    return gulp.src('./app/**/*.html')
        .pipe(gulp.dest('dist/app'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('app/**/*.js', ['lint', 'review', 'build-index']);
    gulp.watch('assets/stylesheets/**/*.css', ['build-index']);
});

// Remove build folder
gulp.task('clean', function() {
    gulp.src('./dist/*')
        .pipe(clean({force: true}));
});

// Default Task
gulp.task('default', ['lint', 'review', 'build-index', 'watch']);

gulp.task('build', function() {
  runSequence(
    ['clean'],
    ['lint', 'review', 'build-index']
  );
});

gulp.task('build-production', ['move-index', 'move-node-modules', 'move-bower-components',
          'minify-styles', 'minify-scripts', 'move-images', 'move-html'], function() {

    var target = gulp.src('./dist/index.html');
    var sources = gulp.src(['./vendor/js/angular.js',
                            './vendor/js/ui-bootstrap.js',
                            './vendor/js/**/*.js',
                            './vendor/css/**/*.css',
                            './all.js',
                            './css/**/*.css'], {read: false, cwd: __dirname + '/dist'});

    return target.pipe(inject(sources))
        .pipe(gulp.dest('./dist'));

});
