# Budget Web

Administrative app for Budget.

## Styleguide
Refer to AngularJS John Papa Styleguide: https://github.com/johnpapa/angular-styleguide

## Set Up
NPM and bower have to be installed in order to set up the project.

Run bower and npm to get all dependencies needed.
```sh
  npm install
  bower install
``` 

## Running the app
Run `npm start` for preview.

## Adding new library
When adding new library with either npm or bower use the `--save` or `--save-dev` option.
Also add the new file's path to the gulp task `build-index` in the sources array specifically.
Then run the following command to inject those files into the index `gulp build-index`

## To add the files to the index
When adding a new component to the project, use `gulp build-index` to add the files into the `index.html` file. 

## Testing
Running `karma start` will run the unit tests with karma.

## Deploying to heroku
To deploy the app to heroku follow the next steps:
* Create a new branch
* Run `gulp clean`
* Run `build-production` 
* `git commit -m "new release"`
* `git push -f heroku branch_name:master`