(function() {

  angular
    .module('app.core')
    .controller('ConfirmDeletionModalController', ConfirmDeletionModalController);

  /* @ngInject */
  function ConfirmDeletionModalController($uibModalInstance) {

    var vm = this;
    vm.cancel = cancel;
    vm.confirm = confirm;

    function confirm() {
      $uibModalInstance.close();
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

  }

})();

