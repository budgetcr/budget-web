(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('deletionModalService', deletionModalService);

  /* @ngInject */
  function deletionModalService($uibModal) {

    var service = {
      open: open
    };

    return service;

    function open() {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/shared/confirm_delete_modal.html',
        controller: 'ConfirmDeletionModalController',
        controllerAs: 'vm',
        size: 'sm'
      });

      return modalInstance;
    }

  }

})();

