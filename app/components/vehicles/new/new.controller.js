(function() {
  'use strict';

  angular
    .module('app.vehicles')
    .controller('NewVehicleController', EditVehicleController);

  /* @ngInject */
  function EditVehicleController($state, vehiclesService) {
    var vm = this;
    vm.vehicle = {};
    vm.vehicle.gas = 'Gasolina';
    vm.vehicle.traction_type = '4x2';
    vm.vehiclesService = vehiclesService;
    vm.persistData = persistData;

    function persistData(vehicle) {
      vm.vehiclesService.create(vehicle)
        .then(function() {
          $state.go('vehicle', {id: vehicle.MVA});
        });
    }

  }

})();

