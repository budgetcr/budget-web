(function() {
  'use strict';

  angular
    .module('app.vehicles')
    .controller('EditVehicleController', EditVehicleController);

  /* @ngInject */
  function EditVehicleController($state, deletionModalService, $stateParams, vehiclesService) {
    var vm = this;
    vm.persistData = persistData;
    vm.openDeleteModal = openDeleteModal;

    vehiclesService.findById($stateParams.id)
      .then(function(vehicle) {
        vm.vehicle = vehicle;
      });

    function persistData(vehicle) {
      vehicle.$save()
        .then(function() {
          $state.go('vehicle', {id: vehicle.$id});
        });
    }

    function openDeleteModal() {
      var modalInstance = deletionModalService.open();

      modalInstance.result
        .then(function(success) {
          vehiclesService.deleteVehicle(vm.vehicle).then(function() {
            $state.go('vehicles');
          });
        });
    }

  }

})();

