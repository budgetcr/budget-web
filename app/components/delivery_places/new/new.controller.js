(function() {
  'use strict';

  angular
    .module('app.deliveryPlaces')
    .controller('NewDeliveryPlaceController', NewDeliveryPlaceController);

  /* @ngInject */
  function NewDeliveryPlaceController(deliveryPlacesService, $state) {

    var vm = this;

    vm.save = save;

    function save(deliveryPlace) {
      deliveryPlacesService.save(deliveryPlace)
        .then(function() {
          $state.go('deliveryPlaces');
        });
    }

  }

})();

