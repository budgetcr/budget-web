(function() {
  'use strict';

  angular
    .module('app.deliveryPlaces')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('deliveryPlaces', {
        url: '/delivery_places',
        templateUrl: 'app/components/delivery_places/delivery_places.html',
        controller: 'DeliveryPlacesController as vm'
      })
      .state('deliveryPlace', {
        url: '/delivery_place/:id',
        templateUrl: 'app/components/delivery_places/show/show.html',
        controller: 'ShowDeliveryPlaceController as vm'
      })
      .state('deliveryPlaceNew', {
        url: '/delivery_places/new',
        templateUrl: 'app/components/delivery_places/new/new.html',
        controller: 'NewDeliveryPlaceController as vm'
      });

  }

})();

