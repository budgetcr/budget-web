(function() {
  'use strict';

  angular
    .module('app.deliveryPlaces', [
      'app.core'
    ]);

})();

