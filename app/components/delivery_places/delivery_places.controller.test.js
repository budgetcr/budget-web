(function() {
  'use strict';

  describe('DeliveryPlacesController', function() {

    var controller, deferred;

    beforeEach(function() {

      module(function($provide) {

        $provide.service('authService', function() {
          this.verifyAccess = function() {};
        });

        $provide.service('deliveryPlacesService', function() {
          this.getAll = function() {};
        });

      });

      module('app.deliveryPlaces');

      inject(function($q, $controller, deliveryPlacesService) {

        deferred = $q.defer();
        spyOn(deliveryPlacesService, 'getAll').and.returnValue(deferred.promise);

        controller = $controller('DeliveryPlacesController');

      });

    });

    describe('on instantiation', function() {

      var $rootScope;

      beforeEach(inject(function(_$rootScope_) {
        $rootScope = _$rootScope_;
      }));

      it('gets all the delivery places on instantiation', function() {

        var deliveryPlaces = {name: 'place_name'};
        deferred.resolve(deliveryPlaces);
        $rootScope.$apply();

        expect(controller.deliveryPlaces).toEqual(deliveryPlaces);

      });

    });

    describe('.showDeliveryPlace', function() {

      var $state;

      beforeEach(inject(function(_$state_) {
        $state = _$state_;
      }));

      it('goes to the detail of a delivery place', function() {

        var placeID = '745y43nuvsh98v';

        spyOn($state, 'go');
        controller.showDeliveryPlace(placeID);

        expect($state.go).toHaveBeenCalledWith('deliveryPlace', {id: placeID});

      });

    });

  });

})();

