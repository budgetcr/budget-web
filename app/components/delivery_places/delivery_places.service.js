(function() {
  'use strict';

  angular
    .module('app.deliveryPlaces')
    .factory('deliveryPlacesService', deliveryPlacesService);

  /* @ngInject */
  function deliveryPlacesService($firebaseArray, $firebaseObject) {

    var rootRef = firebase.database().ref();

    var service = {
      get: get,
      getAll: getAll,
      remove: remove,
      save: save
    };

    return service;

    function get(id) {
      var deliveryPlaceRef = rootRef.child('delivery_places').child(id);
      return $firebaseObject(deliveryPlaceRef).$loaded();
    }

    function getAll() {
      var deliveryPlacesRef = rootRef.child('delivery_places');
      return $firebaseArray(deliveryPlacesRef).$loaded();
    }

    function remove(id) {
      return rootRef.child('delivery_places').child(id).remove();
    }

    function save(deliveryPlace) {
      return rootRef.child('delivery_places').push(deliveryPlace);
    }

  }

})();

