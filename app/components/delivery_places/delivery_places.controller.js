(function() {
  'use strict';

  angular
    .module('app.deliveryPlaces')
    .controller('DeliveryPlacesController', DeliveryPlacesController);

  /* @ngInject */
  function DeliveryPlacesController(deliveryPlacesService, $state) {

    var vm = this;

    vm.showDeliveryPlace = showDeliveryPlace;

    activate();

    function activate() {
      deliveryPlacesService.getAll()
        .then(function(deliveryPlaces) {
          vm.deliveryPlaces = deliveryPlaces;
        });
    }

    function showDeliveryPlace(id) {
      $state.go('deliveryPlace', {id: id});
    }

  }

})();

