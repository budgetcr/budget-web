(function() {
  'use strict';

  angular
    .module('app.deliveryPlaces')
    .controller('ShowDeliveryPlaceController', ShowDeliveryPlaceController);

  /* @ngInject */
  function ShowDeliveryPlaceController($state, $stateParams, deliveryPlacesService) {

    var vm = this;

    vm.remove = remove;

    deliveryPlacesService.get($stateParams.id)
      .then(function(deliveryPlace) {
        vm.deliveryPlace = deliveryPlace;
      });

    function remove() {
      deliveryPlacesService.remove($stateParams.id)
        .then(function() {
          $state.go('deliveryPlaces');
        });
    }


  }

})();

