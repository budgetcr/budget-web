(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('UsersController', UsersController);

  /* @ngInject */
  function UsersController(usersService, sessionService, $state) {

    var vm = this;
    vm.users = {};
    vm.showUser = showUser;
    vm.fetchUsers = fetchUsers;
    vm.shouldShowCreateAdminUser = shouldShowCreateAdminUser;

    fetchUsers();

    function fetchUsers(){
      usersService.getAll()
        .then(function(users) {
          vm.users = users;
        });
    }

    function showUser(userId) {
      $state.go('user', { id: userId });
    }

    function shouldShowCreateAdminUser() {
      var session = sessionService.getAuthData();
      var adminUser = "admin@budget.com";
      var result = false;
      if (session.user.email == adminUser) {
        result = true;
      }
      return result;
    }

  }

})();

