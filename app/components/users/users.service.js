(function() {
  'use strict';

  angular
    .module('app.users')
    .factory('usersService', usersService);

  /* @ngInject */
  function usersService($firebaseObject) {

    var rootRef = firebase.database().ref();
    var service = {
      getAll: getAll,
      findById: findById,
      deleteUser: deleteUser,
      create: create
    };

    return service;

    function getAll() {
      return $firebaseObject(rootRef.child('users')).$loaded();
    }

    function findById(id) {
      return $firebaseObject(rootRef.child('users').child(id)).$loaded();
    }

    function create(user) {
      var result = null;

      if (user.type == 'appUser') {
        var newPath = rootRef.child('users').child(user.username);
        result = newPath.set(user);
      } else {
        var userEmail = user.username + '@budget.com';
        result = rootRef.createUser({ email: userEmail, password: user.password})
      }
      return result;
    }

    function deleteUser(user) {
      return user.$remove();
    }

  }

})();

