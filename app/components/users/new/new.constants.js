(function() {
  'use strict';

  angular
    .module('app.users')
    .constant('ERROR_MESSAGES', [
      {error: 'EMAIL_ALREADY_IN_USER', message: 'Correo ya está en uso', text: 'The specified email address is already in use'},
      {error: 'PASSWORD_LENGTH', message: 'Contraseña debe ser mayor a 6 caractéres', text: 'The specified email address is already in use'}
    ]);

})();
