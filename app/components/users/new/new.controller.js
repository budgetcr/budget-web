(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('NewUserController', NewUserController);

  /* @ngInject */
  function NewUserController($state, $stateParams, usersService, ngNotify, ERROR_MESSAGES, lodash) {
    var vm = this;
    var userType = $stateParams.type;

    vm.user = {};
    vm.user.type = userType;
    vm.usersService = usersService;
    vm.persistData = persistData;
    vm.checkPassword = checkPassword;

    function persistData(user) {
      vm.usersService.create(user)
        .then(function(iud) {
          if(angular.isDefined(iud)) {
            $state.go('users');
          } else {
            $state.go('user', { id: user.username });
          }
        })
        .catch(function(error) {
          ngNotify.set(getErrorMessage(error), 'error');
        });
    }

    function getErrorMessage(error) {
      var message = "";
      for(var i = 0; i < ERROR_MESSAGES.length; i++) {
        if (_.includes(error.message, ERROR_MESSAGES[i].text)) {
          message = ERROR_MESSAGES[i].message;
          break;
        }
      }
      return message;
    }

    function checkPassword() {
      return (vm.user.password == vm.passwordConfirmation && !(vm.user.password == null));
    }
  }

})();

