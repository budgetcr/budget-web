(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('EditUserController', EditUserController);

  /* @ngInject */
  function EditUserController($state, $stateParams, usersService, deletionModalService) {
    var vm = this;
    vm.user = {};
    vm.persistData = persistData;
    vm.fetchUser = fetchUser;
    vm.openDeleteModal = openDeleteModal;
    fetchUser($stateParams.id);

    function fetchUser(userId) {
      usersService.findById(userId).then(function(user) {
        vm.user = user;
      });

    }

    function persistData(user) {
      user.$save()
        .then(function() {
          $state.go('user', { id: user.$id });
        });
    }

    function openDeleteModal() {
      var modalInstance = deletionModalService.open();

      modalInstance.result
        .then(function(success) {
          usersService.deleteUser(vm.user).then(function() {
            $state.go('users');
          });
        });
    }

  }

})();

