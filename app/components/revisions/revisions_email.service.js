(function() {
  'use strict';

  angular
    .module('app.revisions')
    .factory('revisionsEmailService', revisionsEmailService);

  /* @ngInject */
  function revisionsEmailService(REVISIONS_URL, $http, $filter) {

    var service = {
      url: REVISIONS_URL,
      sendEmail: sendEmail,
      formatRevisionToJson: formatRevisionToJson,
      getNewObservations: getNewObservations,
      formatDamagesToJson: formatDamagesToJson,
      getComment: getComment
    };

    return service;

    function sendEmail(revision, comment, email, ccMail, language) {
      return $http.post(service.url, service.formatRevisionToJson(revision, comment, email, ccMail, language), { "Content-Type": "application/json" });
    }

    function formatRevisionToJson(revision, comment, email, ccMail, language) {
      var dateFilter = $filter('date');
      var damages = revision.damages;
      var observations = revision.observations;
      var json = {
        damages: service.formatDamagesToJson(damages),
        observations: service.getNewObservations(observations),
        vehicleType: revision.vehicleType,
        revision: {
          gasLevel: revision.gas_level,
          deliveryPlace: revision.delivery_place,
          km: revision.km,
          timestamp: dateFilter(Date.now(), 'yyyy-MM-dd HH:mm:ss'),
          type: revision.type,
          username: revision.username,
          vehicleMVA: revision.vehicle_ref,
          carParts: revision.car_parts_present,
          canvas: revision.canvas,
          license_plate: revision.license_plate
        },
        ccMail: ccMail,
        email: email,
        language: language,
        comment: service.getComment(comment, language),
        signature: false,
        contractNumber: revision.contract_number
      };
      return json;
    }

    function formatDamagesToJson(damages) {
      var formmattedDamages = [];
      damages.forEach(function(damage) {
        formmattedDamages.push({ damage: damage.damage_type, part: damage.part, isNew: damage.is_new});
      });
      return formmattedDamages;
    }

    function getNewObservations(observations) {
      var newObservations = [];
      if(observations) {
        observations.forEach(function(observation) {
          newObservations.push(observation.observation);
        });
      }
      if(newObservations.length == 0) newObservations.push('Sin observaciones');
      return newObservations;
    }

    function getComment(comment, language) {
      const noCommentText = (language === "spanish") ? "Sin comentarios" : "No comments";
      return (comment === "") ? noCommentText : comment;
    }

  }

})();
