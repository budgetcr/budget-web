(function() {
  'use strict';

  angular
    .module('app.revisions', [
      'app.core',
      'angularUtils.directives.dirPagination',
      'ngLodash'
    ]);

})();

