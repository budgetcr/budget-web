(function() {
  'use strict';

  angular
    .module('app.revisions')
    .controller('RevisionsController', RevisionsController);

  /* @ngInject */
  function RevisionsController(revisionsService, $state) {
    var vm = this;

    vm.revisionsPerPage = 20;
    vm.largestPageNumber = 0;
    vm.filterString = "";
    vm.nextKey = "";
    vm.pagination = {current: 1};
    vm.revisions = [];
    vm.revisionsBeforeFind = [];
    vm.isFirstSearch = true;
    vm.pageChanged = pageChanged;
    vm.showRevision = showRevision;
    vm.filter = filter;
    
    getRevisionPage(1);

    function getRevisionPage(pageNumber) {
      if(pageNumber > vm.largestPageNumber) {
        vm.isErrorLoad = false;
        vm.loading = true;
        revisionsService.getRevisionPage(vm.revisionsPerPage + 1, vm.nextKey).then(loadRevision).catch(showError);
      }
    }

    function pageChanged(newPage) {
      if(vm.filterString === "") {
        getRevisionPage(newPage);
      }
    }

    function loadRevision(revisions) {
      vm.largestPageNumber = vm.pagination.current;
      if(vm.largestPageNumber > 1) {
        revisions.splice(revisions.length - 1, 1);
      }
      vm.revisions = revisions.concat(vm.revisions);
      vm.nextKey = revisions[0].timestamp;
      vm.loading = false;
    }

    function showError(error) {
      vm.isErrorLoad = true;
      console.log(error);
    }

    function showRevision(revisionId) {
      $state.go('revision', {id: revisionId});
    }

    function filter() {
      vm.loading = true;
      if(vm.filterString !== "") {
        revisionsService.findRevision(vm.filterString).then(function (result) {
          vm.pagination.current = 1;
          if(vm.isFirstSearch) {
            vm.revisionsBeforeFind = vm.revisions;
            vm.isFirstSearch = false;
          }
          vm.revisions = result;
          vm.loading = false;
        });
      }
      else {
        vm.pagination.current = 1;
        vm.revisions = vm.revisionsBeforeFind;
        vm.revisionsBeforeFind = [];
        vm.loading = false;
        vm.isFirstSearch = true;
      }
    }
  }
})();

