(function() {
  'use strict';

  angular
    .module('app.revisions')
    .controller('EditRevisionController', EditRevisionController);

  /* @ngInject */
  function EditRevisionController($state, $stateParams, $q, revisionsService,
                                  deletionModalService, TIRE_BRANDS, REVISION_TYPES,
                                  GAS_STATES, deliveryPlacesService) {

    var vm = this;

    vm.tireBrands = TIRE_BRANDS;
    vm.revisionTypes = REVISION_TYPES;
    vm.gasStates = GAS_STATES;
    vm.currentRevisionId = $stateParams.id;
    vm.createObservation = createObservation;
    vm.saveRevision = saveRevision;
    vm.deleteObservation = deleteObservation;
    vm.openDeleteModal = openDeleteModal;

    revisionsService.getRevision(vm.currentRevisionId)
      .then(function(revision) {
        vm.revision = revision;
        removeEmptyContractNumber();
        if (vm.revision.observations_ref) {
          getObservations(vm.revision.observations_ref);
        } else {
          vm.observations = [];
        }
      });

    deliveryPlacesService.getAll()
      .then(function(deliveryPlaces) {
        vm.deliveryPlaces = deliveryPlaces;
      });

    function removeEmptyContractNumber() {
      if(vm.revision.contract_number == ' ') {
        vm.revision.contract_number = '';
      }
    }

    function getObservations(observationsRef) {
      revisionsService.getObservations(observationsRef)
        .then(function(observations) {
          vm.observations = observations;
        });
    }

    function createObservation() {
      vm.observations.push({
        is_new: true,
        observation: ''
      });
    }

    function deleteObservation(observation) {
      var index = vm.observations.indexOf(observation);
      vm.observations.splice(index, 1);
    }

    function saveRevision() {
      vm.observations = cleanArray(vm.observations, 'observation');
      $q.all([
        revisionsService.saveRevision(vm.revision),
        revisionsService.saveObservations(vm.observations, vm.revision)
      ]).then(function() {
        $state.go('revision', {id: vm.currentRevisionId});
      }).catch(function(error) {
        console.log(error);
      });

    }

    function cleanArray(array, keyRequiredUnempty) {
      var cleanArray = [];
      array.forEach(function(item) {
        if(item[keyRequiredUnempty] != '') cleanArray.push(item);
      });

      array.splice(0, array.length);
      cleanArray.forEach(function(item) {
        array.push(item);
      });
      return array;
    }

    function openDeleteModal() {
      var modalInstance = deletionModalService.open();

      modalInstance.result
        .then(function(success) {
          revisionsService.deleteRevision(vm.revision).then(function() {
            $state.go('revisions');
          });
        });
    }

  }

})();

