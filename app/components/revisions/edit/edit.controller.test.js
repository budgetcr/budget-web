(function() {
  'use strict';

describe('EditRevisionController', function() {

    var $controller, $q, $state, deferredRevision, deferredSaveRef,
        deferredPlaces, revisionsService, ctrl, $rootScope;
    var REVISION_ID = 12572466;

    beforeEach(module('app.core'));
    beforeEach(module('app.revisions'));

    beforeEach(module(function($provide) {
      $provide.service('revisionsService', function() {
        this.getRevision = getRevision;
        this.saveRevision = saveRevision;
        this.saveObservations = saveObservations;
      });

      var getRevision = function(id) {};
      var saveRevision = function(revision) {};
      var saveObservations = function(observations) {};
    }));

    beforeEach(module(function($provide) {
      $provide.constant('TIRE_BRANDS', 'tire brands');
      $provide.constant('REVISION_TYPES', 'revision type');
      $provide.constant('GAS_STATES', 'gas state');

      $provide.service('authService', function() {
        this.logOut = function() {};
        this.isLoggedIn = function() {};
        this.verifyAccess = function() {};
      });

      $provide.service('deliveryPlacesService', function() {
        this.getAll = function() {};
      });
    }));

    beforeEach(inject(
      function(_$controller_, _$q_, _$state_, _revisionsService_, _$rootScope_, deliveryPlacesService) {
        $q = _$q_;
        $state = _$state_;
        revisionsService = _revisionsService_;
        $rootScope = _$rootScope_;
        var stateParams = {id: REVISION_ID};

        deferredRevision = $q.defer();
        spyOn(revisionsService, 'getRevision').and.returnValue(deferredRevision.promise);
        deferredPlaces = $q.defer();
        spyOn(deliveryPlacesService, 'getAll').and.returnValue(deferredPlaces.promise);

        $controller = _$controller_('EditRevisionController', {
          'revisionsService': revisionsService,
          '$state': $state,
          '$stateParams': stateParams
        });
      }
    ));

    it('gets the revision', function() {
      var revision = {id: REVISION_ID};

      deferredRevision.resolve(revision);
      $rootScope.$apply();

      expect(revisionsService.getRevision).toHaveBeenCalledWith(REVISION_ID);
      expect($controller.revision).toEqual(revision);
    });

    describe('.saveRevision', function() {
      it('updates revision and goes back to show page', function() {
        deferredSaveRef = $q.defer();
        var deferredSaveObservationsRef = $q.defer();

        spyOn(revisionsService, 'saveRevision').and.returnValue(deferredSaveRef.promise);
        spyOn(revisionsService, 'saveObservations').and.returnValue(deferredSaveObservationsRef.promise);
        spyOn($state, 'go').and.callFake(function(id) {});

        $controller.observations = [{observation: 'test'}, {observation: 'test'}];
        $controller.saveRevision();

        deferredSaveRef.resolve({});
        deferredSaveObservationsRef.resolve({});
        $rootScope.$apply();

        expect(revisionsService.saveRevision).toHaveBeenCalledWith($controller.revision);
        expect(revisionsService.saveObservations).toHaveBeenCalledWith($controller.observations, $controller.revision);
        expect($state.go).toHaveBeenCalledWith('revision', {id: REVISION_ID});
      });
    });

  });

})();

