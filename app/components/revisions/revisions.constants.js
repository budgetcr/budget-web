(function() {
  'use strict';

  angular
    .module('app.revisions')
    .constant('TIRE_BRANDS', [
      {name: 'Dunlop'},
      {name: 'Bridgestone'},
      {name: 'Yokohama'},
      {name: 'Firestone'},
      {name: 'Pirelli'},
      {name: 'Kumho'},
      {name: 'Hankook'},
      {name: 'Goodyear'},
      {name: 'Michelin'},
      {name: 'Toyo'},
      {name: 'Maxxis'},
      {name: 'Otros'}
    ])
    .constant('REVISION_TYPES', [
      {name: 'check-in'},
      {name: 'check-out'}
    ])
    .constant('CAR_PARTS', [
      {id: 'antenna', name: 'Antena'},
      {id: 'carpet', name: 'Alfombras'},
      {id: 'emblems', name: 'Emblemas'},
      {id: 'emergency_kit', name: 'Kit de Emergencia'},
      {id: 'legal_documents', name: 'Documentos Legales'},
      {id: 'plates', name: 'Placas'},
      {id: 'rack', name: 'Rack'},
      {id: 'tools', name: 'Herramientas'},
      {id: 'hood', name: 'Tapa de Motor'},
      {id: 'trunk', name: 'Cajuela'},
      {id: 'hubcaps', name: 'Copas'}
    ])
    .constant('GAS_STATES', [
      {name: 'Vacio'},
      {name: '1/8'},
      {name: '1/4'},
      {name: '3/8'},
      {name: '1/2'},
      {name: '5/8'},
      {name: '7/8'},
      {name: 'Lleno'}
    ])
    .constant('VEHICLES', [
      {VEHICLE_4X2_URL: 'assets/img/4x2_vehicle-g.png'},
      {VEHICLE_4X4_URL: 'assets/img/4x4_vehicle-g.png'}
    ]);

})();
