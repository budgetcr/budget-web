(function() {

  angular
    .module('app.revisions')
    .controller('SendEmailModalController', SendEmailModalController);

  /* @ngInject */
  function SendEmailModalController($uibModalInstance, revisionsEmailService, revision, comment, addAlert) {

    var vm = this;
    vm.revision = revision;
    vm.comment = comment;
    vm.addAlert = addAlert;
    vm.sendRevision = sendRevision;
    vm.cancel = cancel;

    activate();

    function activate(){
      vm.ccMail = '';
      vm.language = 'spanish';
    }

    function sendRevision(revision, comment, email, ccMail, language) {
      vm.addAlert({ type: 'info', msg: 'Se está procesando el correo' });
      $uibModalInstance.close(revisionsEmailService.sendEmail(revision, comment, email, ccMail, language));
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

  }

})();
