(function() {
  'use strict';

  angular
    .module('app.revisions')
    .factory('showService', showService);

  /* @ngInject */
  function showService(vehiclesService, revisionsService, $q) {

    var service = {
      vehicle: '',
      revision: {},
      loadRevisionData: loadRevisionData,
      setRevision: setRevision,
      setObservations: setObservations,
      setDamages: setDamages,
      getRevision: getRevision,
      setVehicle: setVehicle,
      getVehicle: getVehicle,
      getDamages: getDamages,
      setCanvas: setCanvas,
      getNewObservations: getNewObservations,
      resetRevisionData: resetRevisionData
    };

    return service;

    function loadRevisionData(vehicleId){
      var deferred = $q.defer();
      revisionsService.getRevision(vehicleId)
        .then(function(revision) {
          service.setRevision(revision);
          loadDamagesAndObservations()
            .then(function(){
              vehiclesService.findById(service.getRevision().vehicle_ref)
              .then(function(vehicle) {
                service.setVehicle(vehicle);
                deferred.resolve();
              });
          });
        });
      return deferred.promise;
    }

    function loadDamagesAndObservations() {
      return $q.all([loadDamages(), loadObservations()]);
    }

    function setRevision(revision){
      service.revision = revision;
    }

    function setDamages(damages){
      service.revision.damages = damages;
    }

    function setObservations(observations){
      service.revision.observations = observations;
    }

    function setVehicle(vehicle){
      service.vehicle = vehicle;
    }

    function getVehicle(){
      return service.vehicle;
    }

    function getRevision(){
      return service.revision;
    }

    function getDamages(){
      return service.revision.damages
    }

    function getNewObservations(){
      return service.observations
    }

    function setCanvas(canvas){
     service.revision.canvas = canvas;
    }

    function resetRevisionData(){
      service.vehicle = '';
      service.revision = {};
    }

    function loadDamages(){
      var damagesRef = service.getRevision().damages_ref;

      if (damagesRef){
        return revisionsService.getDamages(damagesRef)
          .then(function(damages) {
            setDamages(damages);
          });
      }
    }

    function loadObservations(){
      var observationsRef = service.getRevision().observations_ref;

      if(observationsRef){
        return revisionsService.getObservations(observationsRef)
          .then(function(observations) {
            setObservations(observations);
          });
      }
    }
  }
})();
