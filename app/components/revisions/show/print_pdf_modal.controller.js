(function() {

  angular
    .module('app.revisions')
    .controller('PrintPDFModalController', PrintPDFModalController);

  /* @ngInject */
  function PrintPDFModalController($uibModalInstance, revisionsPDFService, revision, comment, addAlert) {

    var vm = this;
    vm.revision = revision;
    vm.comment = comment;
    vm.addAlert = addAlert;
    vm.generatePDF = generatePDF;
    vm.cancel = cancel;

    activate();

    function activate(){
      vm.language = 'spanish';
    }

    function generatePDF(revision, comment, language) {
      vm.addAlert({ type: 'info', msg: 'Se está procesando el reporte' });
      $uibModalInstance.close(revisionsPDFService.getPDF(revision, comment, language));
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

  }

})();
