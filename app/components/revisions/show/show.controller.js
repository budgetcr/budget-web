(function() {
  'use strict';

  angular
    .module('app.revisions')
    .controller('ShowRevisionController', ShowRevisionController);

  /* @ngInject */
  function ShowRevisionController($state, $stateParams, $uibModal, $rootScope, $scope, $window, showService, CAR_PARTS, revisionsEmailService, revisionsPDFService) {
    var vm = this;
    vm.revision;
    vm.CAR_PARTS = CAR_PARTS;
    vm.currentRevisionId = $stateParams.id;
    vm.alerts = [];
    vm.editRevision = editRevision;
    vm.addAlert = addAlert;
    vm.closeAlert = closeAlert;
    vm.openEmailModal = openEmailModal;
    vm.goToFeedback = goToFeedback;
    vm.printRevision = printRevision;
    vm.showFeedbackButton = showFeedbackButton;
    vm.isNotANumber = isNotANumber;

    activate();

    function activate() {
      showService.loadRevisionData(vm.currentRevisionId)
        .then(function(){
          vm.revision = showService.getRevision();
          vm.damages = showService.getDamages();
          vm.observations = showService.getNewObservations();
          vm.vehicle = showService.getVehicle();
          $rootScope.$emit('dataIsLoaded');
        });
      $scope.$on("$destroy", function(){
        showService.resetRevisionData();
      });
    }

    function editRevision(revisionId) {
      $state.go('editRevision', {id: revisionId});
    }

    function addAlert(alert) {
      vm.alerts.push(alert);
    }

    function closeAlert(index) {
      vm.alerts.splice(index, 1);
    }

    function goToFeedback(revID) {
      var feedbackId = vm.revision.feedback_ref;
      $state.go('feedbackDetail', {id: feedbackId});
    }

    function openEmailModal() {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/components/revisions/show/send_email_modal.html',
        controller: 'SendEmailModalController',
        controllerAs: 'vm',
        size: 'sm',
        resolve: {
          revision: function () {
            return vm.revision;
          },
          comment: function() {
            return vm.comment;
          },
          addAlert: function() {
            return vm.addAlert;
          }
        }
      });

      modalInstance.result
        .then(function(success) {
          vm.addAlert({ type: 'success', msg: 'Se ha enviado el correo correctamente' });
        })
        .catch(function(error) {
          if(error != 'cancel' && error != 'backdrop click') {
            vm.addAlert({ type: 'danger', msg: 'No se ha podido enviar el correo, trate nuevamente' });
          }
        });
    }

    function printRevision(revID) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/components/revisions/show/print_pdf_modal.html',
        controller: 'PrintPDFModalController',
        controllerAs: 'vm',
        size: 'sm',
        resolve: {
          revision: function () {
            return vm.revision;
          },
          comment: function() {
            return vm.comment;
          },
          addAlert: function() {
            return vm.addAlert;
          }
        }
      });

      modalInstance.result
        .then(function(success) {
          var file = new Blob([success.data], { type: 'application/pdf' });
          var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
          vm.addAlert({ type: 'success', msg: 'Se ha generado el pdf correctamente' });
        })
        .catch(function(error) {
          if(error != 'cancel' && error != 'backdrop click') {
            vm.addAlert({ type: 'danger', msg: 'No se ha podido obtener el PDF.' });
          }
        });
    }

    function showFeedbackButton(feedbackRef) {
      return angular.isDefined(feedbackRef);
    }

    function isNotANumber(value) {
      return isNaN(value);
    }
  }

})();
