(function() {
  'use strict';

  angular
    .module('app.revisions')
    .controller('CanvasController', CanvasController);

  /* @ngInject */
  function CanvasController(VEHICLES, $q, showService, $rootScope) {
    var revisionLayer;
    activate();

    function activate() {
      initPaperLibrary();
      initializeCanvas();
    }

    function initializeCanvas() {
      $rootScope.$on('dataIsLoaded', function(){
        revisionLayer = new Layer();
        importCanvas();
      });
    }

    function scaleImage(raster) {
      var heightScale = (paper.view.size.height / raster.height);
      var widthScale = (paper.view.size.width / raster.width);
      raster.scale(widthScale, heightScale);
      revisionLayer.addChild(raster);
    }

    function setupRaster(){
      var imageSource = (showService.getVehicle().traction_type == '4x4') ?  VEHICLES[1].VEHICLE_4X4_URL : VEHICLES[0].VEHICLE_4X2_URL;
      return new paper.Raster({
        source: imageSource,
        position: paper.view.center
      });
    }

    function setVehicleBackground() {
      var raster = setupRaster();
      showService.getRevision().vehicleType = showService.getVehicle().traction_type;
      var deferred = $q.defer();

      raster.onLoad = function() {
        scaleImage(raster);
        deferred.resolve();
      };
      return deferred.promise;
    }

    function importCanvas() {
      var damages = showService.getDamages();

      setVehicleBackground()
        .then(function () {
          if (damages) {
            addDamagesToCanvas(damages);
          }
        });
    }

    function addDamagesToCanvas(damagesList) {
      angular.forEach(damagesList, function(damage){
        var component = revisionLayer.importJSON(damage.json_canvas);
        component.position.x = (parseFloat(damage.relative_cords.x_percentage) * (paper.view.size.width));
        component.position.y = (parseFloat(damage.relative_cords.y_percentage) * (paper.view.size.height));
      });
      showService.setCanvas(project.exportSVG({asString:true}));
      paper.view.update();
    }

    function initPaperLibrary() {
      paper.install(window);
      paper.setup('canvas');
    }
  }
})();