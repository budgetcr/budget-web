(function() {
  'use strict';

  angular
    .module('app.revisions')
    .factory('revisionsService', revisionsService);

  /* @ngInject */
  function revisionsService($firebaseArray, $firebaseObject, $q, lodash) {

    var service = {
      getDamages: getDamages,
      getRevision: getRevision,
      getRevisionPage: getRevisionPage,
      getObservations: getObservations,
      rootRef: firebase.database().ref(),
      saveObservations: saveObservations,
      deleteRevision: deleteRevision,
      saveRevision: saveRevision,
      findRevision: findRevision
    };

    return service;

    function getRevision(id) {
      var revisionRef = service.rootRef.child('revisions').child(id);
      return $firebaseObject(revisionRef).$loaded();
    }

    function getRevisionPage(revisionAmount, startKey) {
      var revisionsRef = this.rootRef.child('revisions').orderByChild('timestamp').limitToLast(revisionAmount);
      if(startKey !== "") {
        revisionsRef = revisionsRef.endAt(startKey);
      }
      return $firebaseArray(revisionsRef).$loaded();
    }

    function findRevision(keyword) {
      var promises = [];
      var blob = ['username', 'contract_number', 'license_plate', 'delivery_place', 'vehicle_ref', 'type'];
      angular.forEach(blob, function(value) {
        promises.push(findRevisionAux(value, keyword));
      });

      return $q.all(promises).then(reformatFoundRevisions);
    }

    function findRevisionAux(child, keyword) {
      var revisionsRef = service.rootRef.child('revisions').orderByChild(child).equalTo(keyword);
      return $firebaseArray(revisionsRef).$loaded();
    }

    function reformatFoundRevisions(promises) {
      var foundRevisions = [];
      angular.forEach(promises, function (promise) {
          foundRevisions = lodash.union(promise, foundRevisions);
      });
      return foundRevisions;
    }

    function getDamages(ref) {
      var damagesRef = service.rootRef.child('damages').child(ref);
      return $firebaseArray(damagesRef).$loaded();
    }

    function getObservations(ref) {
      var observationsRef = service.rootRef.child('observations').child(ref);
      return $firebaseArray(observationsRef).$loaded();
    }

    function saveObservations(observations, revision) {
      if(observations.$ref && observations.length) {
        var ref = observations.$ref();
        replaceExistingObservations(ref, observations);

      } else if(!observations.$ref && observations.length) {
        var ref = service.rootRef.child('observations').push('');
        replaceExistingObservations(ref, observations);
        revision.observations_ref = ref.key();
        saveRevision(revision);

      } else if(observations.$ref && !observations.length) {
        observations.$ref().remove();
        revision.observations_ref = null;
        saveRevision(revision);
      }
    }

    function saveRevision(revision) {
      return revision.$save();
    }

    function deleteRevision(revision) {
      if(revision.observations_ref) service.rootRef.child('observations').child(revision.observations_ref).remove();
      if(revision.damages_ref) service.rootRef.child('damages').child(revision.damages_ref).remove();
      return revision.$remove();
    }

    function replaceExistingObservations(ref, observations) {
      ref.remove();
      observations.forEach(function(observation) {
        delete observation.$priority;
        delete observation.$id;
        delete observation.$$hashKey;
        ref.push(observation);
      });
    }

  }

})();

