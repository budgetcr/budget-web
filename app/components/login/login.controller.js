(function() {
  'use strict';

  angular
    .module('app.login')
    .controller('LoginController', LoginController);

  /* @ngInject */
  function LoginController(authService, sessionService, $state) {

    var vm = this;

    vm.closeAlert = closeAlert;
    vm.logIn = logIn;

    function closeAlert() {
      vm.isIncorrectLogin = false;
    }

    function logIn(username, password) {
      authService.logIn(username, password)
        .then(function(authData) {
          sessionService.setAuthData(authData);
          authService.redirect();
          vm.isIncorrectLogin = false;
        })
        .catch(function(error) {
          vm.isIncorrectLogin = true;
        });
    }

  }

})();

