(function() {
  'use strict';

  angular
    .module('app.login')
    .factory('authService', authService);

  /* @ngInject */
  function authService($state, $rootScope, $location, sessionService) {

    var service = {
      isLoggedIn: isLoggedIn,
      logIn: logIn,
      logOut: logOut,
      verifyAccess: verifyAccess,
      redirect: redirect
    }

    return service;

    function isLoggedIn() {
      var authData = sessionService.getAuthData();
      var sessionDefined = typeof authData !== 'undefined';
      var authDataDefined = authData !== null;

      return sessionDefined && authDataDefined;
    };

    function logIn(username, password) {
      return firebase.auth().signInWithEmailAndPassword(username + '@budget.com', password);
    }

    function logOut() {
      sessionService.destroy();
      $state.go('login');
    }

    function verifyAccess() {
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {

        if(!service.isLoggedIn() && toState.name !== 'login') {
          service.redirectInfo = {};
          service.redirectInfo.toStateName = toState.name;
          service.redirectInfo.toParams = toParams;
          $location.path('login');
        }

      });
    }

    function redirect() {
      if(service.redirectInfo) {
        $state.go(service.redirectInfo.toStateName, service.redirectInfo.toParams);
      } else {
        $state.go('revisions');
      }
    }

  };

})();

