(function() {
  'use strict';

  angular
    .module('app')
    .config(configuration);

  /* @ngInject */
  function configuration() {
    // Initialize Firebase
    var stagingConfig = {
      apiKey: "AIzaSyAk5uI1me3mx5IqLEc_Een2M4ybtAOhfuE",
      authDomain: "budget-cr-staging.firebaseapp.com",
      databaseURL: "https://budget-cr-staging.firebaseio.com",
      projectId: "budget-cr-staging",
      storageBucket: "budget-cr-staging.appspot.com",
      messagingSenderId: "327629040301"
    };
    var devConfig = {
      apiKey: "AIzaSyB0s_8HZvnVziyhPCx_rPrxXTBs0Ceucxs",
      authDomain: "budget-cr-dev.firebaseapp.com",
      databaseURL: "https://budget-cr-dev.firebaseio.com",
      projectId: "budget-cr-dev",
      storageBucket: "budget-cr-dev.appspot.com",
      messagingSenderId: "896877567573"
    };
    firebase.initializeApp(devConfig);
  }

})();

