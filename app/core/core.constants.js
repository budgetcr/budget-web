(function() {
  'use strict';

  var BASE_PATH    = 'https://budget-cr-ws-prod.herokuapp.com';
  var TESTING_PATH = 'http://localhost:8080';

  angular	
    .module('app.core')
    .constant('FIREBASE_URL', 'https://budget-cr.firebaseio.com')
    .constant('REVISIONS_URL', BASE_PATH + '/revisions')
    .constant('PDF_URL', BASE_PATH + '/pdf');
})();
