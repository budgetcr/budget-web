(function() {
  'use strict';

  angular
    .module('app', [
      'app.core',
      'app.vehicles',
      'app.layout',
      'app.users',
      'app.revisions',
      'app.deliveryPlaces',
      'app.login'
    ]);

})();
